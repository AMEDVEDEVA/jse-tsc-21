package ru.tsc.golovina.tm.service;

import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.entity.UserEmailExistsException;
import ru.tsc.golovina.tm.exception.entity.UserLoginExistsException;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new UserLoginExistsException(login);
        final User user = new User(login, HashUtil.salt(password));
        repository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        final User user = create(login, password);
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(password));
        return user;
    }

    @Override
    public User setRole(final String id, final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public User findUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User removeUserById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public User removeUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        return userRepository.findUserByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(final String email) {
        return userRepository.findUserByEmail(email) != null;
    }

    @Override
    public User updateUserById(
            final String id, final String lastName, final String firstName,
            final String middleName, final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        final User user = repository.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateUserByLogin(
            final String login, final String lastName, final String firstName,
            final String middleName, final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExists(email)) throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

}