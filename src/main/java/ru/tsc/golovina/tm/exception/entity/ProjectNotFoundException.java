package ru.tsc.golovina.tm.exception.entity;

import ru.tsc.golovina.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
