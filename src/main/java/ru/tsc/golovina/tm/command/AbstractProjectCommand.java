package ru.tsc.golovina.tm.command;

import ru.tsc.golovina.tm.exception.empty.EmptyNameException;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
