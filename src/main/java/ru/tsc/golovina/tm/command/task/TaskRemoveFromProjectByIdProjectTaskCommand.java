package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveFromProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String getCommand() {
        return "task-remove-from-project-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Removes task from project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
    }

}
