package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUpdateByIdCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update user info by id";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth) throw new AccessDeniedException();
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUserById(id, lastName, firstName, middleName, email);
    }

}
