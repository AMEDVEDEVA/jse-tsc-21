package ru.tsc.golovina.tm.command.user;

import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class UserDisplayByLoginCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-display-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display user by login";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        showUser(user);
    }

}
