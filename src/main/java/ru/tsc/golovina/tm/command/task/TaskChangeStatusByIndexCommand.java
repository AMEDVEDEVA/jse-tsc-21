package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-change-status-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change task status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        serviceLocator.getTaskService().changeStatusByIndex(userId, index, status);
    }

}
