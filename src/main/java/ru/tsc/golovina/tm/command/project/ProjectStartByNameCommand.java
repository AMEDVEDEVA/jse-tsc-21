package ru.tsc.golovina.tm.command.project;

import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-start-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectService().startByName(userId, name);
    }

}
