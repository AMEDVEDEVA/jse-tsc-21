package ru.tsc.golovina.tm.command.task;

import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-show-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
