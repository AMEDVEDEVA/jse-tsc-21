package ru.tsc.golovina.tm.command.project;

import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-show-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}
